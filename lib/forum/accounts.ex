defmodule Forum.Accounts do
  @moduledoc """
  The Accounts context.
  """

  import Ecto.Query, warn: false
  alias Forum.Repo
  alias Forum.Accounts.User
  alias Forum.Accounts.UserToken
  alias Forum.Accounts.Profile

  def get_resource_by_id(id), do: Repo.get(User, id)

  def get_user_by_email(email) do
    User
    |> where(email: ^email)
    |> Repo.one()
    |> case do
      nil -> :error
      user -> {:ok, user}
    end
  end

  def create_accounts(params) do
    params
    |> User.signup_changeset()
    |> Repo.insert()
  end

  def deliver_confirmation(user) do
    {encode_token, user_token} = UserToken.build_email_token(user, "confirm")
    Repo.insert(user_token)

    Forum.Email.deliver_confirmation_instructions(
      user,
      Forum.Validators.confirm_url(:confirm, encode_token)
    )
  end

  def confirm_user(token) do
    UserToken
    |> where(token: ^token)
    |> Repo.one()
  end

  def delete_token(email) do
    UserToken
    |> where(sent_to: ^email)
    |> Repo.delete_all()
  end

  def verified_user(email) do
    User
    |> where(email: ^email)
    |> Repo.one()
    |> User.verified_changeset(%{verified: true})
    |> Repo.update()
  end

  def update_password(user, params) do
    user
    |> User.password_changeset(params)
    |> Repo.update()
  end

  def update_profile(profile, params) do
    profile
    |> Profile.changeset(params)
    |> Repo.update()
  end
end
