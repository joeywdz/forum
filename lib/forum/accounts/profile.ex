defmodule Forum.Accounts.Profile do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "profiles" do
    field :avatar, :string, default: "http://dummyimage.com/400x400"
    field :bio, :string, default: "No personal introduction yet"
    field :username, :string
    belongs_to :user, Forum.Accounts.User

    timestamps()
  end

  @doc false
  def changeset(profile, attrs) do
    profile
    |> cast(attrs, [:avatar, :bio, :username])
    |> validate_required([:avatar, :bio, :username])
  end
end
