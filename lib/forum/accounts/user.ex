defmodule Forum.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "users" do
    field :email, :string
    field :password, :string, virtual: true
    field :hash_password, :string
    field :verified, :boolean, default: false

    has_one :profile, Forum.Accounts.Profile
    has_many :article, Forum.Articles.Article
    has_many :favorite, Forum.Favorites.Favorite

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:email, :hash_password, :verified])
    |> validate_required([:email, :hash_password, :verified])
  end

  def signup_changeset(params) do
    %__MODULE__{}
    |> cast(params, [:email, :password])
    |> validate_email()
    |> validate_password()
    |> cast_assoc(:profile)
  end

  defp validate_email(changeset) do
    changeset
    |> validate_required([:email])
    |> Forum.Validators.validate_email(:email)
    |> unsafe_validate_unique(:email, Forum.Repo)
    |> unique_constraint(:email)
  end

  defp validate_password(changeset) do
    changeset
    |> validate_required([:password])
    |> validate_length(:password, min: 12, max: 80)
    |> hash_password()
  end

  defp hash_password(changeset) do
    case get_change(changeset, :password) do
      nil ->
        changeset

      password ->
        changeset
        |> put_change(:hash_password, Bcrypt.hash_pwd_salt(password))
        |> delete_change(:password)
    end
  end

  def verified_changeset(user, params) do
    user
    |> cast(params, [:verified])
    |> validate_required([:verified])
  end

  def password_changeset(user, params) do
    user
    |> cast(params, [:password])
    |> validate_password()
  end
end
