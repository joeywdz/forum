defmodule Forum.Accounts.UserToken do
  use Ecto.Schema
  # import Ecto.Changeset

  @rand_size 32

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "user_tokens" do
    field :context, :string
    field :sent_to, :string
    field :token, :binary
    belongs_to :user, Forum.Accounts.User

    timestamps()
  end

  def build_email_token(user, context) do
    build_hashed_token(user.id, context, user.email)
  end

  defp build_hashed_token(user_id, context, sent_to) do
    token = :crypto.strong_rand_bytes(@rand_size)
    hashed_token = Phoenix.Token.sign(ForumWeb.Endpoint, context, token)

    {Base.url_encode64(hashed_token, padding: false),
     %Forum.Accounts.UserToken{
       token: token,
       context: context,
       sent_to: sent_to,
       user_id: user_id
     }}
  end
end
