defmodule Forum.Articles.Article do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "articles" do
    field :content, :string
    field :title, :string
    belongs_to :user, Forum.Accounts.User
    field :category_id, :binary_id

    has_many :comment, Forum.Comments.Comment

    timestamps()
  end

  @doc false
  def changeset(article, attrs) do
    article
    |> cast(attrs, [:title, :content, :user_id, :category_id])
    |> validate_required([:title, :content, :user_id, :category_id])
  end
end
