defmodule Forum.Comments.Comment do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "comments" do
    field :content, :string
    field :user_id, :binary_id
    # field :article_id, :binary_id
    belongs_to :article, Forum.Articles.Article

    timestamps()
  end

  @doc false
  def changeset(comment, attrs) do
    comment
    |> cast(attrs, [:content, :user_id, :article_id])
    |> validate_required([:content, :user_id, :article_id])
  end
end
