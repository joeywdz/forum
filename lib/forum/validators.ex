defmodule Forum.Validators do
  import Ecto.Changeset

  def validate_email(changeset, field) do
    changeset
    |> validate_format(field, ~r/^[^\s]+@[^\s]+$/, message: "must have the @ sign and no spaces")
    |> validate_length(field, max: 160)
  end

  def confirm_url(type, token) do
    url = "http://localhost:4000"

    case type do
      :confirm ->
        "#{url}/confirm/#{token}"

      :reset ->
        "#{url}/reset/#{token}"
    end
  end
end
