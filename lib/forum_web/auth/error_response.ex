defmodule ForumWeb.Auth.ErrorResponse.Unauthorized do
  defexception message: "Unauthorized", plug_status: 401
end

defmodule ForumWeb.Auth.ErrorResponse.BadRequest do
  defexception message: "badrequest", plug_status: 400
end
