defmodule ForumWeb.Auth.Pipeline do
  use Guardian.Plug.Pipeline,
    otp_app: :forum,
    module: ForumWeb.Auth.Guardian,
    error_handler: ForumWeb.Auth.GuardianErrorHandler

  plug Guardian.Plug.VerifySession
  plug Guardian.Plug.VerifyHeader
  plug Guardian.Plug.EnsureAuthenticated
  plug Guardian.Plug.LoadResource
end
