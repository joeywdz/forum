defmodule ForumWeb.Auth.SetAccount do
  import Plug.Conn
  alias Forum.Repo

  def init(_opts), do: :ok

  def call(conn, _opts) do
    case conn.assigns[:user] do
      nil ->
        user =
          Repo.preload(conn.private.guardian_default_resource, [:profile, :article, :favorite])

        assign(conn, :user, user)

      _user ->
        conn
    end
  end
end
