defmodule ForumWeb.AccountController do
  use ForumWeb, :controller

  alias Forum.Accounts
  alias Forum.Accounts.User
  alias ForumWeb.Auth.Guardian
  alias ForumWeb.Auth.ErrorResponse
  action_fallback ForumWeb.FallbackController

  def signup(conn, %{"user" => params}) do
    with {:ok, user} <- Accounts.create_accounts(params) do
      Accounts.deliver_confirmation(user)

      conn
      |> put_status(:created)
      |> render(:show_user, user: user)
    end
  end

  def signin(conn, %{"email" => email, "password" => password}) do
    with {:ok, %User{verified: true} = user} <- Accounts.get_user_by_email(email),
         {:ok, _, token} <- Guardian.authenticate(email, password) do
          IO.inspect(wasm_test())
      conn
      |> put_status(:ok)
      |> render(:user_token, %{user: user, token: token})
    else
      {:ok, %User{verified: false}} ->
        raise ErrorResponse.Unauthorized, message: "Email not confirmed"

      _ ->
        raise ErrorResponse.Unauthorized, message: "Email or password incorrect"
    end
  end

  def signin(_conn, _) do
    raise ErrorResponse.BadRequest, message: "Missing email and/or password"
  end

  def current(conn, %{}) do
    conn
    |> put_status(:ok)
    |> render(:show, user: conn.assigns[:user])
  end

  def refresh_token(conn, %{}) do
    token = Guardian.Plug.current_token(conn)
    {:ok, user, new_token} = Guardian.authenticate(token)

    conn
    |> put_status(:ok)
    |> render(:user_token, %{user: user, token: new_token})
  end

  def logout(conn, _params) do
    user = conn.assigns[:user]
    token = Guardian.Plug.current_token(conn)
    Guardian.revoke(token)

    conn
    |> put_status(:ok)
    |> render(:user_token, %{user: user, token: nil})
  end

  def update_password(conn, %{
        "current_password" => current_password,
        "password_params" => password_params
      }) do
    user = conn.assigns[:user]

    with true <- Guardian.validate_password(current_password, user.hash_password),
         {:ok, _user} <- Accounts.update_password(user, password_params),
         token <- Guardian.Plug.current_token(conn),
         {:ok, user, new_token} = Guardian.authenticate(token) do
      conn
      |> put_status(:ok)
      |> render(:user_token, %{user: user, token: new_token})
    else
      _error -> raise ErrorResponse.Unauthorized, message: "Update password failed"
    end
  end

  def update_profile(conn, %{"profile" => profile_params}) do
    user = conn.assigns[:user]

    case Accounts.update_profile(user.profile, profile_params) do
      {:ok, profile} ->
        conn
        |> put_status(:ok)
        |> render(:profile, profile: profile)

      _error ->
        raise ErrorResponse.Unauthorized, message: "Update profile failed"
    end
  end

  defp wasm_test() do
    args = [
      %{
        "field_type" => "single_line_field",
        "type" => "literal",
        "value" => "Asia/Shanghai"
      }
    ]

    args_binary = Jason.encode!(args)

    {:ok, outputs} =
             AirbaseSandbox.Program.run(args_binary,
               program_loader: fn ->
                 File.read("/Users/joey/Documents/graduation/airbase_sandbox/test/fixtures/networking-sample.wasm")
               end
             )

    map = outputs |> Jason.decode!() |> List.first()
    map["value"]
  end
end
