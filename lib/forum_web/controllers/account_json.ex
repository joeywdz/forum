defmodule ForumWeb.AccountJSON do
  def show_user(%{user: user}) do
    %{
      id: user.id,
      email: user.email,
      profile: profile(%{profile: user.profile})
    }
  end

  def show(%{user: user}) do
    %{
      id: user.id,
      email: user.email,
      profile: profile(%{profile: user.profile}),
      article: ForumWeb.ArticleJSON.index(%{articles: user.article}),
      favorite: ForumWeb.FavoriteJSON.index(%{favorites: user.favorite})
    }
  end

  def user(%{user: user}) do
    %{
      id: user.id,
      email: user.email
    }
  end

  def profile(%{profile: profile}) do
    %{
      id: profile.id,
      username: profile.username,
      bio: profile.bio,
      avatar: profile.avatar
    }
  end

  def user_token(%{user: user, token: token}) do
    %{
      id: user.id,
      email: user.email,
      token: token
    }
  end
end
