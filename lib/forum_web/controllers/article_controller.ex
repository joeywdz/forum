defmodule ForumWeb.ArticleController do
  use ForumWeb, :controller
  alias Forum.Articles

  action_fallback ForumWeb.FallbackController

  def create(conn, %{"article" => article_params}) do
    with params <- Map.put(article_params, "user_id", conn.assigns[:user].id),
         {:ok, article} <- Articles.create_article(params) do
      conn
      |> put_status(:created)
      |> render(:show, article: article)
    end
  end

  def index(conn, _params) do
    with articles <- Articles.list_articles() do
      conn
      |> put_status(:ok)
      |> render(:index, articles: articles)
    end
  end

  def show(conn, %{"id" => id}) do
    with article <- Articles.get_article!(id),
         article <- Forum.Repo.preload(article, :comment) do
      conn
      |> put_status(:ok)
      |> render(:show_comment, article: article)
    end
  end

  def update(conn, %{"article_id" => article_id, "article" => article_params}) do
    with article <- Articles.get_article!(article_id),
         true <- if_user(article.user_id, conn.assigns[:user].id),
         {:ok, article} <- Articles.update_article(article, article_params) do
      conn
      |> put_status(:ok)
      |> render(:show, article: article)
    end
  end

  def delete(conn, %{"id" => article_id}) do
    with article <- Articles.get_article!(article_id),
         true <- if_user(article.user_id, conn.assigns[:user].id),
         {:ok, article} <- Articles.delete_article(article) do
      conn
      |> put_status(:ok)
      |> render(:show, article: article)
    end
  end

  defp if_user(article_user_id, current_user_id) do
    if article_user_id == current_user_id do
      true
    else
      false
    end
  end
end
