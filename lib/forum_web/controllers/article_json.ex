defmodule ForumWeb.ArticleJSON do
  alias Forum.Categories
  alias Forum.Accounts

  def index(%{articles: articles}) do
    %{data: for(article <- articles, do: show(%{article: article}))}
  end

  def show(%{article: article}) do
    user = Accounts.get_resource_by_id(article.user_id) |> Forum.Repo.preload(:profile)
    category = Categories.get_category!(article.category_id)

    %{
      id: article.id,
      title: article.title,
      content: article.content,
      category_id: article.category_id,
      category: category.name,
      user_id: article.user_id,
      email: user.email,
      username: user.profile.username,
      create_time: article.inserted_at,
      update_time: article.updated_at
    }
  end

  def show_comment(%{article: article}) do
    user = Accounts.get_resource_by_id(article.user_id) |> Forum.Repo.preload(:profile)
    category = Categories.get_category!(article.category_id)

    %{
      id: article.id,
      title: article.title,
      content: article.content,
      category_id: article.category_id,
      category: category.name,
      user_id: article.user_id,
      email: user.email,
      comments: ForumWeb.CommentJSON.index(%{comments: article.comment}),
      username: user.profile.username,
      create_time: article.inserted_at,
      update_time: article.updated_at
    }
  end
end
