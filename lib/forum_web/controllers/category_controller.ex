defmodule ForumWeb.CategoryController do
  use ForumWeb, :controller

  action_fallback ForumWeb.FallbackController

  def create(conn, %{"category" => category_params}) do
    with {:ok, category} <- Forum.Categories.create_category(category_params) do
      conn
      |> put_status(:created)
      |> render(:show, category: category)
    end
  end

  def index(conn, %{}) do
    with categories <- Forum.Categories.list_categories() do
      conn
      |> put_status(:ok)
      |> render(:index, categories: categories)
    end
  end
end
