defmodule ForumWeb.CategoryJSON do
  def index(%{categories: categories}) do
    %{data: for(category <- categories, do: show(%{category: category}))}
  end

  def show(%{category: category}) do
    %{
      id: category.id,
      name: category.name,
      description: category.description
    }
  end
end
