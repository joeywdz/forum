defmodule ForumWeb.CommentController do
  use ForumWeb, :controller
  alias Forum.Comments
  action_fallback ForumWeb.FallbackController

  def create(conn, %{"comment" => comment}) do
    with params <- Map.put(comment, "user_id", conn.assigns[:user].id),
         {:ok, comment} <- Comments.create_comment(params) do
      conn
      |> put_status(:created)
      |> render(:show, comment: comment)
    end
  end

  def index(conn, %{"id" => article_id}) do
    with comments <- Comments.list_comments_index(article_id) do
      conn
      |> put_status(:ok)
      |> render(:index, comments: comments)
    end
  end
end
