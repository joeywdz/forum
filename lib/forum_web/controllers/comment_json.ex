defmodule ForumWeb.CommentJSON do
  alias Forum.Accounts

  def index(%{comments: comments}) do
    %{data: for(comment <- comments, do: show(%{comment: comment}))}
  end

  def show(%{comment: comment}) do
    user = Accounts.get_resource_by_id(comment.user_id) |> Forum.Repo.preload(:profile)

    %{
      id: comment.id,
      content: comment.content,
      user_id: comment.user_id,
      email: user.email,
      username: user.profile.username,
      article_id: comment.article_id,
      create_time: comment.inserted_at,
      update_time: comment.updated_at
    }
  end
end
