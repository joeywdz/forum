defmodule ForumWeb.ConfirmController do
  use ForumWeb, :controller

  alias Forum.Accounts

  action_fallback ForumWeb.FallbackController

  def confirm(conn, %{"token" => url_token}) do
    case Phoenix.Token.verify(
           ForumWeb.Endpoint,
           "confirm",
           Base.decode64!(url_token, padding: false),
           max_age: 86_400
         ) do
      {:ok, token} ->
        ok_token(conn, token)

      {:error, errors} ->
        error_token(conn, errors)
    end
  end

  defp ok_token(conn, token) do
    case Accounts.confirm_user(token) do
      nil ->
        {:error, :invalid}

      user_token ->
        with {:ok, _} <- Accounts.verified_user(user_token.sent_to),
             _ <- Accounts.delete_token(user_token.sent_to) do
          conn
          |> put_status(:ok)
          |> json(%{message: "confirmed"})
        end
    end
  end

  defp error_token(conn, errors) do
    conn
    |> put_status(:unprocessable_entity)
    |> json(%{errors: errors})
  end
end
