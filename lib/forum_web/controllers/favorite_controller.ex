defmodule ForumWeb.FavoriteController do
  use ForumWeb, :controller
  action_fallback ForumWeb.FallbackController

  def create(conn, favorite) do
    with params <- Map.put(favorite, "user_id", conn.assigns[:user].id),
         {:ok, favorite} <- Forum.Favorites.create_favorite(params) do
      conn
      |> put_status(:ok)
      |> render(:show, favorite: favorite)
    end
  end

  def index(conn, %{}) do
    conn
    |> put_status(:ok)
    |> render(:index, favorites: conn.assigns[:user].favorite)
  end

  def delete_favorite(conn, %{"id" => id}) do
    IO.inspect(id)

    with params <- Forum.Favorites.get_favorite!(id),
         {:ok, favorite} <- Forum.Favorites.delete_favorite(params) do
      conn
      |> put_status(:ok)
      |> render(:show, favorite: favorite)
    end
  end
end
