defmodule ForumWeb.FavoriteJSON do
  def index(%{favorites: favorites}) do
    %{data: for(favorite <- favorites, do: show(%{favorite: favorite}))}
  end

  def show(%{favorite: favorite}) do
    article = Forum.Articles.get_article!(favorite.article_id)

    %{
      id: favorite.id,
      article: article.title,
      article_id: favorite.article_id
    }
  end
end
