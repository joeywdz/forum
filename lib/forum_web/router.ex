defmodule ForumWeb.Router do
  use ForumWeb, :router
  use Plug.ErrorHandler

  def handle_errors(conn, params) do
    handle_errors_format(conn, params)
  end

  defp handle_errors_format(conn, %{reason: %Phoenix.Router.NoRouteError{message: message}}) do
    conn |> json(%{errors: message}) |> halt()
  end

  defp handle_errors_format(conn, %{reason: %{message: message}}) do
    conn |> json(%{errors: message}) |> halt()
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :auth do
    plug ForumWeb.Auth.Pipeline
    plug ForumWeb.Auth.SetAccount
  end

  ## 登陆和注册
  scope "/api", ForumWeb do
    pipe_through :api
    post "/accounts/signup", AccountController, :signup
    post "/accounts/signin", AccountController, :signin
  end

  ## 用户操作相关
  scope "/api", ForumWeb do
    pipe_through [:api, :auth]
    get "/accounts/current", AccountController, :current
    get "/accounts/refresh_token", AccountController, :refresh_token
    get "/accounts/logout", AccountController, :logout
    put "/accounts/update_password", AccountController, :update_password
    put "/accounts/update_profile", AccountController, :update_profile
  end

  ## 需要token得文章操作相关
  scope "/api", ForumWeb do
    pipe_through [:api, :auth]
    post "/articles", ArticleController, :create
    put "/articles", ArticleController, :update
    delete "/articles/:id", ArticleController, :delete
  end

  ## 不需要token得文章操作
  scope "/api", ForumWeb do
    pipe_through :api
    get "/articles", ArticleController, :index
    get "/articles/:id", ArticleController, :show
    get "/articles/:id/comments", CommentController, :index
    get "/categories", CategoryController, :index
    post "/categories", CategoryController, :create
  end

  ## 评论
  scope "/api", ForumWeb do
    pipe_through [:api, :auth]
    post "/comments", CommentController, :create
  end

  ## 收藏表
  scope "/api", ForumWeb do
    pipe_through [:api, :auth]
    get "/favorites/:article_id", FavoriteController, :create
    get "/favorites", FavoriteController, :index
    delete "/favorites/:id", FavoriteController, :delete_favorite
  end

  scope "/", ForumWeb do
    pipe_through :api
    get "/confirm/:token", ConfirmController, :confirm
  end

  # Enable LiveDashboard and Swoosh mailbox preview in development
  if Application.compile_env(:forum, :dev_routes) do
    # If you want to use the LiveDashboard in production, you should put
    # it behind authentication and allow only admins to access it.
    # If your application does not have an admins-only section yet,
    # you can use Plug.BasicAuth to set up some basic authentication
    # as long as you are also using SSL (which you should anyway).
    import Phoenix.LiveDashboard.Router

    scope "/dev" do
      pipe_through [:fetch_session, :protect_from_forgery]

      live_dashboard "/dashboard", metrics: ForumWeb.Telemetry
      forward "/mailbox", Plug.Swoosh.MailboxPreview
    end
  end
end
