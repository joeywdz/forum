defmodule Forum.Repo.Migrations.CreateFavorites do
  use Ecto.Migration

  def change do
    create table(:favorites, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :user_id, references(:users, on_delete: :delete_all, type: :binary_id)
      add :article_id, references(:articles, on_delete: :nothing, type: :binary_id)

      timestamps()
    end

    create index(:favorites, [:user_id])
    create index(:favorites, [:article_id])
  end
end
