defmodule Forum.AccountsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Forum.Accounts` context.
  """

  @doc """
  Generate a user.
  """
  def user_fixture(attrs \\ %{}) do
    {:ok, user} =
      attrs
      |> Enum.into(%{
        email: "some email",
        hash_password: "some hash_password",
        verified: true
      })
      |> Forum.Accounts.create_user()

    user
  end

  @doc """
  Generate a profile.
  """
  def profile_fixture(attrs \\ %{}) do
    {:ok, profile} =
      attrs
      |> Enum.into(%{
        avatar: "some avatar",
        bio: "some bio",
        username: "some username"
      })
      |> Forum.Accounts.create_profile()

    profile
  end

  @doc """
  Generate a user_token.
  """
  def user_token_fixture(attrs \\ %{}) do
    {:ok, user_token} =
      attrs
      |> Enum.into(%{
        context: "some context",
        sent_to: "some sent_to",
        token: "some token"
      })
      |> Forum.Accounts.create_user_token()

    user_token
  end
end
