defmodule Forum.ArticlesFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Forum.Articles` context.
  """

  @doc """
  Generate a article.
  """
  def article_fixture(attrs \\ %{}) do
    {:ok, article} =
      attrs
      |> Enum.into(%{
        content: "some content",
        title: "some title"
      })
      |> Forum.Articles.create_article()

    article
  end
end
