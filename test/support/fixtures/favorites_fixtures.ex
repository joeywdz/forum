defmodule Forum.FavoritesFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Forum.Favorites` context.
  """

  @doc """
  Generate a favorite.
  """
  def favorite_fixture(attrs \\ %{}) do
    {:ok, favorite} =
      attrs
      |> Enum.into(%{})
      |> Forum.Favorites.create_favorite()

    favorite
  end
end
